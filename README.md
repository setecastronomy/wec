# Warning and Error Collector

WEC is a little utitlity which acts as a wrapper over another executable (and its parameters) which captures standard error stream output and does something specified with it, like:

- print immediately (optionally filtered by patterns coming from a specified file)
- not print at all (i.e. discard STDERR outputs)
- store a copy to some files (paths created following specified hints)
- print previously stored results (discovering files following the same hints,
  optionally applying filtering just like for immediate printing)

## USAGE

The regular call of the tool with some arguments will run the tool with all
remaning options passed to the call. The standard output is passed through to
the caller, the error output might be passed through OR processed as mentioned
above and then optionally printed (via STDERR).

The resulting return code of the called application is passed as the own return code.

## Special Modes

### Second wrapper

Since WEC is supposed to be installed like a compiler launcher, it also allows
to run yet another wrapper (a launcher like ccache) with all remaning program
arguments passed through. For the name of that launcher, it looks for a special
`WEC_COMPILER_LAUNCHER` variable.

### Printing of collected outputs

The plain call of the tool **while** `WEC_STORE_PREFIX` is set but no arguments
are passed activates the printing mode. In that mode, `WEC` would look for
previsously dumped error outputs which fit the filenames (wildcard globbing),
and print them.

## User provided instructions

The hints and instructions are passed by environment variables as listed below.kv

- **WEC_MODE** - string, one of run\_print, run\_hide, old\_print. That defines the wanted type of operation, where:
  - run\_print is printing STDERR as it comes (with or without storing, see `WEC_STORE_PREFIX`),
  - run\_hide (optionally) stores STDERR data in the file and does not print it,
  - old\_print is a automatically looking for the stored files (resolving the names by heuristics based on `WEC_STORE_PREFIX`) and prints them.
- **WEC_STORE_PREFIX** - location to store temp. data, like
  /home/foo/tmp/EDUMP_ (that is a base prefix, specific strings are attached to the filenames). There is **NO** automated cleanup of the dumped files, therefore the user is responsible for removing them. Also, for multiple
  calls, the prefix should be unique but reproducible, e.g. a common random
  number or including the ID of some shared process (like the one calling them all,
  like make, Ninja, some wrapper script, etc.).
- **WEC_COMPILER_LAUNCHER** - (see above)
- **WEC_KEY_FILE_PATTERN** - a regex which defines what's considered the output file name or some other significant string within the call arguments that is presented as the result name in the STDERR dumps.
- **WEC_FILTER_FILE** - filename with filter patterns (see below for syntax and implementation details). This affects the printing with run\_print or old\_print. Those patterns which are considered "justified" and to be ignored or shown in the output. This feature is **unstable**, the type of patterns is subject to change.
- **WEC_RETRY_PATTERN** - enables a special function to restart the whole operation one or multiple times in case where the specified pattern (a regular expression) has been found inside of the command outputs on STDERR.
- **WEC_RETRY_OPTS** - additional tuning of the **WEC_RETRY_PATTERN** behavior. The default value is a retry interval of 3 seconds and total timeout of 55 seconds.
  The content of this variable shall be a list where the first character declares the delimiter (of the list elements) and the elements are `key=value` pairs (for examples, use `--help` option). The meaning of the fields (keys) is: `tout` (total timeout of retries, in seconds), `int` (fixed interval, in seconds), `amin` (adaptive mode, in seconds, starting with that interval, increasing by a factor on each retry), `afac` (multiplication factor, to increase the adaptive interval), `amax` (limit of the interval in adaptive mode).
- **BS_PFX** - special variable checked before all others. If set, it will
  replace the "WEC" part of the other variables, i.e. amend their expected
  names.

## Display content filtering

The file declared by `..._FILTER_FILE` variable may contain a list or regular expressions. The first one matching the particular line decides on when to print it or not. The regex line needs to be prefixed with `+`(instruction to show) or with `-` (instruction to hide). A leading `#` character may declare a comment line which has no meaning for processing.

## Exit Status

When a wrapped program is run, reports its program status. If an internal error happens in delicate conditions, a non-zero code is reported and the cause might be printed.

Otherwise, one of those:

- 2, 3: errors on accessing or reading the previously stored files while trying to print them
- 19: lack of arguments implied printing but the storage base was not specified
- 17: internal error while analyzing the state of wrapped process

