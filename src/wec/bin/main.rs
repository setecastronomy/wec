use std::collections::hash_map::DefaultHasher;
use std::fs::{self, File};
//use chrono::{DateTime, Utc};
use std::io::{BufRead, BufReader, BufWriter, ErrorKind, IoSlice, Read, Write};
use std::path::Path;
//use std::str::from_utf8;
//use std::os::raw;
use regex::{Regex, RegexBuilder};
use std::panic;
use std::process::{exit, Command, Stdio};
use std::time::{Duration, Instant, SystemTime};
use std::{env, hash::*};
use std::{env::*, i32};
//use regex_automata::Regex; // should be faster and smaller and easier to handle on pure STDERR stream data -- but lacks features like most important anchors :-()

//const RC_ERR_UNKNOWN_LOCATION :i32 = 19;
const RC_ERR_PROCESS_FAILURE: i32 = 17;
const RC_ERR_STORE_ACCESS: i32 = 2;
const RC_ERR_STORE_READ: i32 = 3;
const RC_ERR_PANIC: i32 = 23;
const RC_VALUE_ERROR: i32 = 29;

const BS_PFX: &str = "BS_PFX";
const VAR_STORE_PREFIX: &str = "_STORE_PREFIX";
const VAR_MODE: &str = "_MODE";
const VAR_FILTER_FILE: &str = "_FILTER_FILE";
const VAR_COMPILER_LAUNCHER: &str = "_COMPILER_LAUNCHER";
const VAR_RETRY_OPTS: &str = "_RETRY_OPTS";
const VAR_RETRY_PAT: &str = "_RETRY_PATTERN";
const VAR_KEY_FILE_PATTERN: &str = "_KEY_FILE_PATTERN";

//const DATE_FORMAT_STR: &'static str = "[%Y-%m-%d][%H:%M:%S]";

macro_rules! if_or_else {
    ($_condition: expr, $_if_yes: expr, $_if_no: expr) => {
        if $_condition {
            $_if_yes
        } else {
            $_if_no
        }
    };
}

//#[repr(u8)]
#[derive(PartialEq)]
enum ECaptureMode {
    Drop,
    CaptureAndReturn,
    PrintOnly,
}

type TDisplayFilter = Vec<(bool, Regex)>;

fn main() {
    setup_abort_hook();

    let opts = TEnvOpts::new();
    let mut ret_opts = TRetryControl::new(&opts);

    loop {
        let rc = command_invocation(&opts);
        // good, or no retry wanted nor possible?
        if rc.0 == 0 || !rc.1 || opts.opt_retry_pat.is_none() {
            exit(rc.0);
        } else {
            ret_opts.delay(rc.0);
        }
    }
}

struct TRetryControl {
    rint: i32,
    ramin: i32,
    rafac: i32,
    ramax: i32,
    rtout: i32,
    extime: Option<Instant>,
}

impl TRetryControl {
    fn new(opts: &TEnvOpts) -> TRetryControl {
        let mut ret = TRetryControl {
            rint: 0,
            ramin: 0,
            rafac: 2,
            ramax: 128,
            rtout: 55,
            extime: None,
        };

        if opts.retry_opts.is_none() && opts.opt_retry_pat.is_some() {
            ret.rint = 3;
        }

        if opts.retry_opts.is_some() && opts.opt_retry_pat.is_some() {
            let ro = opts.retry_opts.clone().unwrap();
            let del = ro.chars().next().unwrap();

            let mut mapping = [
                ("int=", &mut ret.rint),
                ("amin=", &mut ret.ramin),
                ("afac=", &mut ret.rafac),
                ("amax=", &mut ret.ramax),
                ("tout=", &mut ret.rtout),
            ];

            for x in ro[1..].split(del) {
                let mut found = false;
                for kv in &mut mapping {
                    if let Some(rest) = x.strip_prefix(kv.0) {
                        *kv.1 = rest.parse::<i32>().unwrap();
                        found = true;
                        break;
                    }
                }
                if !found {
                    eprintln!(
                        "WARNING: unsupported key=value in {}{VAR_RETRY_OPTS}: {x}",
                        opts.s_bs_pfx
                    );
                }
            }

            if ret.rint == 0 && ret.ramin == 0 {
                eprint!("ERROR: Both fix interval int and adaptive interval amin are zero.");
                exit(RC_VALUE_ERROR)
            }
        }

        ret.extime = Some(Instant::now() + Duration::from_secs(ret.rtout as u64));
        ret
    }

    fn delay(&mut self, last_code: i32) {
        // okay, retry, can we?
        if Instant::now() > self.extime.unwrap() {
            exit(last_code);
        }

        let msg_or_die_if_unreachable_or_sleep = |pause: i32| {
            let now = Instant::now();
            // if going to timeout anyway -> terminate right now
            let dur = Duration::from_secs(pause as u64);
            if now + dur > self.extime.unwrap() {
                exit(last_code);
            }
            let remaining: Duration = self.extime.unwrap() - now;
            let remaining = remaining.as_secs();

            eprintln!("WARNING: transient error detected, will retry the operation for at most {remaining}s, pausing for {pause}s");
            std::thread::sleep(dur);
        };

        if self.rint > 0 {
            msg_or_die_if_unreachable_or_sleep(self.rint);
        } else if self.ramin > 0 {
            msg_or_die_if_unreachable_or_sleep(self.ramin);

            self.ramin *= self.rafac;
            if self.ramin > self.ramax {
                self.ramin = self.ramax;
            }
        }
        // double check, since the process might have been suspended
        if Instant::now() > self.extime.unwrap() {
            exit(last_code);
        }
    }
}

#[derive(Default)]
struct TEnvOpts {
    s_bs_pfx: String,
    mode: Option<String>,
    store_pfx: Option<String>,
    filter_file: Option<String>,
    retry_opts: Option<String>,
    opt_retry_pat: Option<String>,
    ext_launcher: Option<String>,
    result_file_selector: Option<String>,
}

impl TEnvOpts {
    fn new() -> TEnvOpts {
        use std::ffi::OsString;
        let mut ret = TEnvOpts {
            mode: Some("run_print".to_string()),
            ..Default::default()
        };

        let s_bs_pfx = var(BS_PFX).unwrap_or("WEC".to_string());
        let mkey = |x| OsString::from(s_bs_pfx.clone() + x);
        let to_opt_not_empty = |s: &OsString| {
            let s = s.to_string_lossy().to_string();
            if_or_else!(s.is_empty(), None, Some(s))
        };

        let mut mapping = [
            (mkey(VAR_MODE), &mut ret.mode),
            (mkey(VAR_STORE_PREFIX), &mut ret.store_pfx),
            (mkey(VAR_FILTER_FILE), &mut ret.filter_file),
            (mkey(VAR_RETRY_OPTS), &mut ret.retry_opts),
            (mkey(VAR_RETRY_PAT), &mut ret.opt_retry_pat),
            (mkey(VAR_COMPILER_LAUNCHER), &mut ret.ext_launcher),
            (mkey(VAR_KEY_FILE_PATTERN), &mut ret.result_file_selector),
        ];

        for kv in env::vars_os() {
            for n2ref in mapping.iter_mut() {
                if n2ref.0 == kv.0 {
                    *n2ref.1 = to_opt_not_empty(&kv.1);
                    break;
                }
            }
        }
        ret
    }
}

fn command_invocation(opts: &TEnvOpts) -> (i32, bool) {
    let retry_detector = {
        if opts.opt_retry_pat.is_none() {
            None
        } else {
            Some(
                RegexBuilder::new(opts.opt_retry_pat.as_ref().unwrap().as_str())
                    .build()
                    .unwrap(),
            )
        }
    };
    let check_retry = retry_detector.is_some();

    let with_retry_hint = |edata_rc: (Vec<u8>, i32)| -> (i32, bool) {
        let (stderr_data, rc) = edata_rc;
        if rc == 0 || stderr_data.is_empty() || retry_detector.is_none() {
            // nothing we could or should decide on
            return (rc, false);
        }
        let retry_detector = retry_detector.unwrap();
        for rline in stderr_data.split_inclusive(|el| el == &b'\n') {
            if retry_detector.is_match(&String::from_utf8_lossy(rline)) {
                return (rc, true);
            }
        }
        (rc, false)
    };

    match opts.mode.as_ref().unwrap().as_str() {
        "run_print" => {
            let edata_rc = call_prog_and_capture(
                opts,
                if_or_else!(
                    opts.filter_file.is_some() || opts.store_pfx.is_some() || check_retry,
                    ECaptureMode::CaptureAndReturn,
                    ECaptureMode::PrintOnly
                ),
            );
            if opts.store_pfx.is_some() {
                save_eout(opts, &edata_rc.0);
            }
            print_stderr_data(&edata_rc.0, false, &load_display_filter(&opts.filter_file));
            with_retry_hint(edata_rc)
        }
        "run_drop" => with_retry_hint(call_prog_and_capture(
            opts,
            if_or_else!(
                check_retry,
                ECaptureMode::CaptureAndReturn,
                ECaptureMode::Drop
            ),
        )),
        "run_hide" => {
            let edata_rc = call_prog_and_capture(opts, ECaptureMode::CaptureAndReturn);
            if opts.store_pfx.is_some() {
                save_eout(opts, &edata_rc.0);
            };
            with_retry_hint(edata_rc)
        }
        "old_print" => {
            if opts.store_pfx.is_none() {
                eprintln!(
                    "old_print mode requires {}_STORE_PREFIX environment",
                    opts.s_bs_pfx.to_string().as_str()
                );
                (RC_VALUE_ERROR, false)
            } else {
                (print_stored(opts), false)
            }
        }
        _ => {
            eprintln!(
                "Not recognized {}_MODE value: {}",
                opts.s_bs_pfx,
                opts.mode.clone().unwrap_or_default()
            );
            (RC_VALUE_ERROR, false)
        }
    }
}

fn setup_abort_hook() {
    // take_hook() returns the default hook in case when a custom one is not set
    let orig_hook = panic::take_hook();
    panic::set_hook(Box::new(move |panic_info| {
        // invoke the default handler and exit the process
        orig_hook(panic_info);
        std::process::exit(RC_ERR_PANIC);
    }));
}

fn help() {
    print!(
        "WEC - Warning/Error collector
Option(s):
-h\tPrint this help
NOTE: most behavior control happens through environment variables!
Examples:
- WEC{VAR_MODE} (any of: run_print,run_hide,run_drop,old_print)
- WEC{VAR_STORE_PREFIX}='/tmp/stored_errors/example_'
- WEC{VAR_FILTER_FILE}=$PWD/justified_error_patterns.txt
- WEC{VAR_COMPILER_LAUNCHER}=/bin/another_launcher/like_ccache.exe
- WEC{VAR_RETRY_PAT}='(Aborted-by-the-cloud-...-because-some-random-BS|other-BS)'
- WEC{VAR_RETRY_OPTS}=,i=3,a=2,af=2,am=30,max=200
"
    );
    exit(0);
}

fn print_stored(opts: &TEnvOpts) -> i32 {
    let mut rc = 0;
    let pattern = opts.store_pfx.clone().unwrap_or_default() + "*.err";
    let matches = glob::glob(pattern.as_str());
    if matches.is_err() {
        return 0;
    }
    let filter = load_display_filter(&opts.filter_file);
    for opath in matches.unwrap() {
        if opath.is_err() {
            eprintln!("Cannot access {}, ignored it", opath.err().unwrap());
            rc = RC_ERR_STORE_ACCESS;
            continue;
        }
        let path = opath.unwrap();
        let mut fd = match std::fs::File::open(&path) {
            Ok(x) => x,
            Err(ex) => {
                eprintln!(
                    "Unable to open {}, ignored it. ({})",
                    &path.to_string_lossy(),
                    ex
                );
                rc = RC_ERR_STORE_READ;
                continue;
            }
        };
        let mut buf: Vec<u8> = Vec::new();
        if fd.read_to_end(&mut buf).unwrap() == 0 {
            continue;
        }
        print_stderr_data(&buf, true, &filter);
    }
    rc
}

fn print_stderr_data(
    stderr_data: &[u8],
    fst_line_is_header: bool,
    filter: &Option<TDisplayFilter>,
) {
    if filter.is_none() {
        // then don't care about lines, just dump the content and be done with it
        std::io::stderr().write_all(stderr_data).expect("IO error");
        return;
    }

    let mut header: Option<&[u8]> = None;

    // unfortunatelly stderr_data may contain garbage and not just UTF-8 strings, therefore handling that explicitly
    for raw_line in stderr_data.split_inclusive(|el| el == &b'\n') {
        if fst_line_is_header && header.is_none() {
            header = Some(raw_line);
            continue;
        }

        let norm_line = String::from_utf8_lossy(raw_line);
        let line = norm_line.trim();
        let mut do_show = false;
        for filter in filter.as_ref().unwrap() {
            if filter.1.is_match(line) {
                do_show = filter.0;
                break;
            }
        }

        if do_show {
            if header.is_some() {
                //eprintln!("{}", header.unwrap());
                std::io::stderr()
                    .write_all(header.unwrap())
                    .expect("IO error");
                header = None;
            }
            std::io::stderr().write_all(raw_line).expect("IO error");
        }
    }
}

fn load_display_filter(filter_file: &Option<String>) -> Option<TDisplayFilter> {
    if filter_file.is_none() {
        return None;
    }
    let fname = filter_file.as_ref().unwrap().as_str();
    let fd = std::fs::File::open(fname).unwrap_or_else(|_| {
        panic!(
            "Unable to open specified filter configuration, check {}.",
            &fname
        )
    });
    let rdr = BufReader::new(fd);
    let v_content: Vec<(bool, Regex)> = rdr
        .lines()
        .filter_map(|line| {
            let _iline = line.expect("IO error?");
            let iline = _iline.trim();
            if iline.starts_with('#') {
                return None;
            }
            let is_show_filter = iline.starts_with('+');
            let re_input = if is_show_filter || iline.starts_with('-') {
                &iline[1..]
            } else {
                iline
            };
            let rex = Regex::new(re_input.trim_start())
                .unwrap_or_else(|ex| panic!("Bad justification regexp, check {}\n{}", iline, ex));
            Some((is_show_filter, rex))
        })
        .collect();
    if v_content.is_empty() {
        return None;
    };
    //let all_same = v_content.iter().all(|el| el.0 == v_content[0].0);
    Some(v_content)
}

// returns: collected output, return code, calculated hash of the arguments
fn call_prog_and_capture(opts: &TEnvOpts, cap_mode: ECaptureMode) -> (Vec<u8>, i32) {
    let mut args = std::env::args();
    args.next(); // skip us

    let mut cmd = {
        if let Some(prog) = args.next() {
            if prog.as_str() == "-h" || prog.as_str() == "--help" {
                help();
            };
            if opts.ext_launcher.is_none() {
                Command::new(prog)
            } else {
                let mut g = Command::new(opts.ext_launcher.clone().unwrap());
                g.arg(prog);
                g
            }
        } else {
            exit(0);
            //panic!("Required a program to run in this mode but no program was specified");
        }
    };
    cmd.args(args); // remaining arguments
    cmd.stdout(Stdio::inherit());
    cmd.stderr(match cap_mode {
        ECaptureMode::Drop => Stdio::null(),
        ECaptureMode::PrintOnly => Stdio::inherit(),
        ECaptureMode::CaptureAndReturn => Stdio::piped(),
    });
    // pass the stdout through, only pick stderr
    let ret = cmd
        .stdout(Stdio::inherit())
        .output()
        .unwrap_or_else(|x| panic!("Failed to run wrapped compiler process {:?}:\n{}", cmd, x));
    let rc = ret.status.code().unwrap_or(RC_ERR_PROCESS_FAILURE);
    // there is nothing to store or print anyway
    if ret.stderr.is_empty() {
        exit(rc);
    }
    (
        if_or_else!(cap_mode == ECaptureMode::Drop, [].to_vec(), ret.stderr),
        rc,
    )
}

///
/// Stores the specified (raw) blob under certain filename. The output name is calculated based on opts.store_pfx, using some pseudo-unique suffix and .err extension.
/// The file content itself may contain a prefix (header) which annotates the origin of the dump.
/// That header is typically an object file name, which is located in the command line arguments, but might be some different argument if matched
/// by a regex in opts.print_file_pattern.
///
fn save_eout(opts: &TEnvOpts, stderr_strings: &[u8]) {
    let mut oline = String::new();
    let mut dig = DefaultHasher::new();
    let key_file_rex = Regex::new(
        &opts
            .result_file_selector
            .clone()
            .unwrap_or(r"\.(o|obj)$".to_string()),
    )
    .expect("Bad key file name pattern");

    {
        let mut args = std::env::args();
        args.next(); // skip us
        for arg in args {
            dig.write(arg.as_bytes());
            //if oline.is_empty() && (arg.ends_with(".o") || arg.ends_with(".obj")) { oline = arg; }
            if oline.is_empty() && key_file_rex.is_match(arg.as_str()) {
                oline = arg;
            }
        }
    }

    let csum = dig.finish();

    // no object file picked, can we do something about it?
    if oline.is_empty() {
        //oline = format!("{csum}");
        // enough for now - alternative: assemble the whole command line as string; FIXME: what to do with newlines there? Just strip them?
        let mut args_again = std::env::args();
        args_again.next();
        let string_args: Vec<String> = args_again
            .map(|f| f.as_str().to_string().replace('\n', "\\n"))
            .collect();
        oline = string_args.join(" ");
    }

    oline += ":\n";
    //print!("got oline? {}", oline);

    let now = std::time::SystemTime::now();
    let delta = now.duration_since(SystemTime::UNIX_EPOCH).unwrap();
    // we need roughly something like "milliseconds within a day" here
    let secs = delta.as_secs();
    let subsecs = delta.subsec_nanos() / 1024;
    let efile = format!(
        "{}{:x}.{:x}.{:x}.err",
        &opts.store_pfx.clone().unwrap_or_default(),
        secs,
        subsecs,
        csum
    );
    {
        let fd = File::create(&efile);
        if fd.is_err() && fd.unwrap_err().kind() == ErrorKind::NotFound {
            let path = Path::new(&efile);
            fs::create_dir_all(path.parent().unwrap()).unwrap_or_else(|_| {
                eprintln!("Warning, failed to create the STDERR storage parent folder!")
            });
        }
        let fd = File::create(efile).expect("Failed to create error storage file");
        {
            // plain fd.write_vectored is buggy on Windows, only the first slice is written!! Using a BufWriter instead.
            let mut bfd = BufWriter::new(fd);
            let res = bfd
                .write_vectored(&[IoSlice::new(oline.as_bytes()), IoSlice::new(stderr_strings)])
                .expect("Failed to store STDERR contents");

            if res > 0 {
                bfd.flush().expect("STDERR storage error");
            }
        }
    }
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_load_filter() {
        //assert_eq!(add(1, 2), 3);
        let filda = load_display_filter(&Some("tests/filpat.txt".to_string()));
        //assert_eq!(filda.is_some());
        assert!(filda.is_some());
        let filters = filda.unwrap();
        assert_eq!(6, filters.len());
        assert_eq!(6, filters.iter().count());
        assert_eq!(3, filters.iter().filter(|x| x.0).count());
        assert_eq!(3, filters.iter().filter(|x| !x.0).count());
        assert!(filters[2].1.is_match("wanted to see"));
        assert!(!filters[2].1.is_match("NOTTHERE"));
    }
}
